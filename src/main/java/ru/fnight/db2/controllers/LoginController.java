package ru.fnight.db2.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import org.mindrot.jbcrypt.BCrypt;
import ru.fnight.db2.Utils;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class LoginController implements Initializable {

    public Label doneLabel;
    public PasswordField passwordText;
    public TextField loginText;
    private Connection conn = null;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        conn = Utils.createConnection();
    }

    public void tryToLogin(String login, String password) {
        String sql = "select passwd from users where login = ?";
        try {
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, login);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                String passwd = resultSet.getString("passwd");
                resultSet.close();
                statement.close();
                if (BCrypt.checkpw(password, passwd)) {
                    doneLabel.setText("Аутентификация успешна");
                } else {
                    doneLabel.setText("Неверные данные для входа");
                }
            } else {
                doneLabel.setText("Неверные данные для входа");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void login(ActionEvent actionEvent) {
        String login = loginText.getText().trim();
        String password = passwordText.getText();

        if (login.length() > 0 && password.length() > 0) {
            tryToLogin(login, password);
        } else {
            doneLabel.setText("Необходимо ввести логин и пароль");
        }

    }
}
