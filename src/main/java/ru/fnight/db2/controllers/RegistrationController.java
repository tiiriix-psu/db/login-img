package ru.fnight.db2.controllers;


import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import org.mindrot.jbcrypt.BCrypt;
import ru.fnight.db2.Utils;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class RegistrationController implements Initializable {

    public TextField loginText;
    public PasswordField passwordText;
    public Label loginErrorLabel;
    public Tooltip loginErrorTooltip;
    public Label passwordErrorLabel;
    public Tooltip passwordErrorTooltip;
    public Label doneLabel;
    public Button registrationButton;

    private Connection conn = null;
    private boolean isLoginGood = false;
    private boolean isPasswordGood = false;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        conn = Utils.createConnection();

        loginText.textProperty().addListener((observable, oldValue, newValue) ->
                onChangeLoginText(newValue));
        passwordText.textProperty().addListener((observable, oldValue, newValue) ->
                onChangePasswordText(newValue));
    }

    private void tryToUnlockDoneButton() {
        if (isLoginGood && isPasswordGood) {
            registrationButton.setDisable(false);
        } else {
            registrationButton.setDisable(true);
        }
    }

    private void loginChanged(int countLogin) {
        if (countLogin > 0) {
            loginErrorLabel.setText("X");
            isLoginGood = false;
            loginErrorTooltip.setText("Пользователь с таким логином уже существует");
        } else {
            loginErrorLabel.setText("✔");
            isLoginGood = true;
            loginErrorTooltip.setText("");
        }
        tryToUnlockDoneButton();
    }

    public void onChangeLoginText(String newLogin) {
        String trimLogin = newLogin.trim();
        if (trimLogin.equals("") || trimLogin.length() > 200) {
            loginErrorLabel.setText("X");
            isLoginGood = false;
            loginErrorTooltip.setText("Недопустимый логин");
            tryToUnlockDoneButton();
            return;
        }
        String sql = "select count(1) user_exist from users where login = ?";
        try {
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, trimLogin);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            loginChanged(resultSet.getInt("user_exist"));
            resultSet.close();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void onChangePasswordText(String newPassword) {
        if (newPassword.replace(" ", "").length() < 8) {
            passwordErrorLabel.setText("X");
            isPasswordGood = false;
            passwordErrorTooltip.setText("Слишком простой пароль");
        } else {
            passwordErrorLabel.setText("✔");
            isPasswordGood = true;
        }
        tryToUnlockDoneButton();
    }

    public void registration(ActionEvent actionEvent) {
        String sql = "insert into users (login, passwd) values (?, ?)";
        String login = loginText.getText().trim();
        String password = BCrypt.hashpw(passwordText.getText(), BCrypt.gensalt());
        try {
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, login);
            statement.setString(2, password);
            statement.execute();
            doneLabel.setText("Пользователь успешно зарегистрирован");
            loginText.setText("");
            passwordText.setText("");
            registrationButton.setDisable(true);
            loginErrorLabel.setText("");
            passwordErrorLabel.setText("");
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
            doneLabel.setText(e.getMessage());
        }
    }
}
